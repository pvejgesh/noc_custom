# ---------------------------------------------------------------------
# Vendor: BDCOM
# OS:     xPON
# ---------------------------------------------------------------------
# Copyright (C) 2007-2019 The NOC Project
# See LICENSE for details
# ---------------------------------------------------------------------

from noc.core.profile.base import BaseProfile


class Profile(BaseProfile):
    name = "BDCOM.xPON"
    pattern_more = [(r"^ --More-- ", " "), (r"\(y/n\) \[n\]", "y\n")]
    pattern_unprivileged_prompt = r"^(?P<hostname>\S+)>"
    pattern_prompt = r"^(?P<hostname>\S+)#"
    pattern_syntax_error = r"% Unknown command"
    command_more = " "
    command_disable_pager = ["terminal length 0", "terminal width 0"]
    command_super = "enable"
    command_enter_config = "config"
    command_leave_config = "exit"
    command_save_config = "write"
    command_exit = "exit"
    config_volatile = ["^%.*?$"]

    def convert_interface_name(self, interface):
        if interface.startswith("f"):
            return interface.replace("f", "FastEthernet")
        elif interface.startswith("TGigaEthernet"):
            return interface.replace("TGigaEthernet", "TGi") 
        elif interface.startswith("10Giga-FX-SFP"):
            return interface.replace("10Giga-FX-SFP", "TGi")
        elif interface.startswith("10Giga-FX"):
            return interface.replace("10Giga-FX", "TGi")
        elif interface.startswith("GigaEthernet"):
            return interface.replace("GigaEthernet", "Gig")
        elif interface.startswith("Giga-Combo-FX-SFP"):
            return interface.replace("Giga-Combo-FX-SFP", "Gig")
        elif interface.startswith("v"):
            return interface.replace("v", "VLAN")
        elif interface.startswith("n"):
            return interface.replace("n", "Null")
        else:
            return interface
        # if interface.startswith("g"):
        #     return "GigaEthernet" + interface[1:]
        # elif interface.startswith("epon"):
        #     return "EPON" + interface[4:]
        # else:
        #     return interface
